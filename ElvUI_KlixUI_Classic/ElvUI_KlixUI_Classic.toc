## Interface: 11305
## Author: Klix, Dlarge
## Version: 0.13
## Title: |cfffe7b2cElvUI|r |cff1784d1Classic|r |cfff960d9KlixUI|r
## Notes: A decorative edit for |cfffe7b2cElvUI|r with many awesome features!
## Notes-deDE: Eine dekorative Erweiterung für ElvUI mit einigen nützlichen Funktionen.
## RequiredDeps: ElvUI
## DefaultState: Enabled
## SavedVariables: KUIDataDB, AddonPanelProfilesDB, AddonPanelServerDB
## SavedVariablesPerCharacter: KUIDataPerCharDB, nodes, fcontinent, Mail_Senders, Mail_Count
## OptionalDeps: SharedMedia, AddOnSkins, DBM, BigWigs, ls_Toasts, XIV_Databar
## X-ElvVersion: 1.27


libs\load_libs.xml
locales\load_locales.xml
core\load_core.xml
media\load_media.xml
defaults\load_defaults.xml
modules\load_modules.xml
layout\layout.lua
Bindings.xml