** v0.13, 13.08.2020**	
* added GameMenueButtons back.		
* check to only style the unitframes if the styling is checked.
* Chat fixes on install.
* small adjustments in KlixUI Profile.
* minor Code cleanup and optimizations.
